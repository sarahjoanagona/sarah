import tkinter as tk
from tkinter import messagebox
import re

class IndocultureApp:
    def __init__(self, root):
        self.root = root
        self.root.title("INDOCULTURE App")
        self.root.configure(bg="#fff")  # Set background color to white

        # Welcome page
        self.show_welcome_page()

    def show_welcome_page(self):
        # Welcome page widgets
        self.label_welcome = tk.Label(self.root, text="Welcome to INDOCULTURE", font=("Arial", 16), bg="#FFF", fg="#FF8C00")  # Orange color
        self.label_welcome.pack(pady=50)

        # After 3 seconds, hide the welcome text and show the login page
        self.root.after(3000, self.show_login_page)

    def show_login_page(self):
        # Clear any previous widgets
        self.clear_widgets()

        # Login page widgets
        self.label_email = tk.Label(self.root, text="Email:", font=("Arial", 12), bg="#FFF")
        self.label_email.pack(pady=5)
        self.entry_email = tk.Entry(self.root, font=("Arial", 12))
        self.entry_email.pack(pady=5)

        self.label_password = tk.Label(self.root, text="Password:", font=("Arial", 12), bg="#FFF")
        self.label_password.pack(pady=5)
        self.entry_password = tk.Entry(self.root, show="*", font=("Arial", 12))
        self.entry_password.pack(pady=5)

        self.btn_login = tk.Button(self.root, text="Login", command=self.login, font=("Arial", 12), bg="#FF8C00", fg="#FFF")  # Dark shade of orange with white text
        self.btn_login.pack(pady=10)

        # Signup option
        self.btn_signup = tk.Button(self.root, text="Signup", command=self.show_signup_page, font=("Arial", 12), bg="#FF8C00", fg="#FFF")  # Dark shade of orange with white text
        self.btn_signup.pack(pady=10)

    def show_signup_page(self):
        # Clear any previous widgets
        self.clear_widgets()

        # Signup page widgets
        self.label_signup = tk.Label(self.root, text="Sign Up", font=("Arial", 16), bg="#FFF", fg="#FF8C00")  # Orange color
        self.label_signup.pack(pady=20)

        self.label_email = tk.Label(self.root, text="Email:", font=("Arial", 12), bg="#FFF")
        self.label_email.pack(pady=5)
        self.entry_email = tk.Entry(self.root, font=("Arial", 12))
        self.entry_email.pack(pady=5)

        self.label_password = tk.Label(self.root, text="Password:", font=("Arial", 12), bg="#FFF")
        self.label_password.pack(pady=5)
        self.entry_password = tk.Entry(self.root, show="*", font=("Arial", 12))
        self.entry_password.pack(pady=5)

        self.btn_signup = tk.Button(self.root, text="Signup", command=self.signup, font=("Arial", 12), bg="#FF8C00", fg="#FFF")  # Dark shade of orange with white text
        self.btn_signup.pack(pady=10)

        # Back option
        self.btn_back = tk.Button(self.root, text="←", command=self.show_login_page, font=("Arial", 12), bg="#FF8C00", fg="#FFF")  # Dark shade of orange with white text
        self.btn_back.pack(pady=10, padx=10, anchor="nw")

    def login(self):
        email = self.entry_email.get()
        password = self.entry_password.get()

        if not self.validate_email(email) or not self.validate_password(password):
            messagebox.showerror("Login Failed", "Invalid email or password format")
        else:
            self.show_home_page()  # Navigate to the homepage

    def signup(self):
        email = self.entry_email.get()
        password = self.entry_password.get()

        if not self.validate_email(email) or not self.validate_password(password):
            messagebox.showerror("Signup Failed", "Invalid email or password format")
        else:
            # Your signup logic goes here
            messagebox.showinfo("Signup Successful", "You have signed up successfully!")
            self.btn_back = tk.Button(self.root, text="←", command=self.show_login_page, font=("Arial", 12), bg="#FF8C00", fg="#FFF")  # Dark shade of orange with white text
            self.btn_back.pack(pady=10, padx=10, anchor="nw")

    def validate_email(self, email):
        # Regular expression for email validation
        email_pattern = re.compile(r"^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,}$")
        return bool(email_pattern.match(email))

    def validate_password(self, password):
        # You can add your password validation logic here
        # For example, you might want to ensure the password has a minimum length
        return len(password) >= 6  # Just an example, you can adjust this condition as needed

    def show_home_page(self):
        # Clear any previous widgets
        self.clear_widgets()

        # Home page widgets
        self.label_search = tk.Label(self.root, text="Search:", font=("Arial", 14), bg="#FFF")
        self.entry_search = tk.Entry(self.root, font=("Arial", 14))
        self.entry_search.bind("<Return>", self.perform_search)  # Bind the search functionality to the Return/Enter key
        self.btn_search = tk.Button(self.root, text="Go", command=self.perform_search, font=("Arial", 12), bg="#FF8C00", fg="#FFF")

        self.label_search.pack(pady=20)
        self.entry_search.pack(pady=20)
        self.btn_search.pack(pady=20)

        # Menu options
        self.menu_frame = tk.Frame(self.root, bg="#FFF")
        self.menu_frame.pack(pady=20)

        self.menu_options = ["Monuments", "Places", "Cuisines"]

        for option in self.menu_options:
            tk.Button(self.menu_frame, text=option, command=lambda o=option: self.show_page(o), font=("Arial", 12), bg="#FF8C00", fg="#FFF").pack(side="left", padx=10)

    def show_page(self, option):
        # Clear any previous widgets
        self.clear_widgets()

        # Page widgets
        tk.Label(self.root, text=f"{option} of INDIA", font=("Arial", 16), bg="#FFF", fg="#FF8C00").pack(pady=50)

        if option.lower() == "monuments":
            self.display_info("Monuments", self.get_monuments_info())
        elif option.lower() == "places":
            self.display_info("Places", self.get_places_info())
        elif option.lower() == "cuisines":
            self.display_info("Cuisines", self.get_cuisines_info())

        # Back button
        self.btn_back = tk.Button(self.root, text="←", command=self.show_home_page, font=("Arial", 12), bg="#FF8C00", fg="#FFF")  # Dark shade of orange with white text
        self.btn_back.pack(pady=10, padx=10, anchor="nw")

    def perform_search(self, event=None):
        search_query = self.entry_search.get().lower()

        # Dictionaries containing information about monuments, places, and cuisines
        monuments_info = self.get_monuments_info()
        places_info = self.get_places_info()
        cuisines_info = self.get_cuisines_info()

        # Check if search query matches any sub-elements in the lists
        found = False
        for info_dict in [monuments_info, places_info, cuisines_info]:
            for item, info_tuple in info_dict.items():
                if search_query == item.lower():
                    # Display information for the matched sub-element
                    self.clear_widgets()
                    info, image_path = info_tuple
                    tk.Label(self.root, text=f"{item.capitalize()}: {info}", font=("Arial", 12), bg="#FFF").pack()
                    image = tk.PhotoImage(file=image_path)
                    tk.Label(self.root, image=image, bg="#FFF").pack()
                    found = True
                    break
            if found:
                break

        # If no sub-element match is found, perform the default search
        if not found:
            self.default_search(search_query)

    def default_search(self, search_query):
        # Display search results
        tk.Label(self.root, text="Search Results:", font=("Arial", 16), bg="#FFF", fg="#FF8C00").pack(pady=20)

        if "monuments" in search_query:
            self.display_info("Monuments", self.get_monuments_info())
        elif "places" in search_query:
            self.display_info("Places", self.get_places_info())
        elif "cuisines" in search_query:
            self.display_info("Cuisines", self.get_cuisines_info())
        else:
            tk.Label(self.root, text="Invalid category. Please enter 'Monuments', 'Places', or 'Cuisines'.", font=("Arial", 12), bg="#FFF").pack()

    def display_info(self, category, info_dict):
        for item, info_tuple in info_dict.items():
            info, image_path = info_tuple
            tk.Label(self.root, text=f"{category}:", font=("Arial", 14), bg="#FFF").pack(pady=10)
            tk.Label(self.root, text=item.capitalize() + ": " + info, font=("Arial", 12), bg="#FFF").pack()
            image = tk.PhotoImage(file=image_path)
            image_label = tk.Label(self.root, image=image, bg="#FFF")
            image_label.image = image  # Keep a reference to the image object
            image_label.pack()

    def get_monuments_info(self):
        return {
            "Taj Mahal": ("The Taj Mahal is an iconic white marble mausoleum located in Agra, India. It was built by the Mughal emperor Shah Jahan in memory of his favorite wife, Mumtaz Mahal, who died during childbirth in 1631. Construction of the Taj Mahal began in 1632 and was completed in 1653, employing thousands of artisans and craftsmen from across the Mughal Empire and beyond.

The Taj Mahal is renowned for its breathtaking beauty and intricate architectural details. Its symmetrical design, with its domed central chamber surrounded by four minarets, is a masterpiece of Mughal architecture. The mausoleum is adorned with intricate marble inlay work, calligraphy, and precious gemstones, creating a stunning display of craftsmanship.

The Taj Mahal is also famous for the love story behind its construction, symbolizing the eternal love between Shah Jahan and Mumtaz Mahal. Today, it is one of the most visited landmarks in the world, attracting millions of tourists annually and standing as a symbol of India's rich cultural heritage and architectural brilliance.




", "taj mahal.png"),
            "Qutub Minar": ("The Qutub Minar is a minaret that forms part of the Qutb complex, a UNESCO World Heritage Site in the Mehrauli area of Delhi, India.", "qutub_minar.png"),
            "Red Fort": ("The Red Fort is a historic fort in the city of Delhi in India that served as the main residence of the Mughal Emperors.", "red_fort.png"),
        }

    def get_places_info(self):
        return {
            "Udaipur": ("Udaipur, formerly the capital of the Mewar Kingdom, is a city in the western Indian state of Rajasthan.", "udaipur.png"),
            "Rajasthan": ("Rajasthan is a state in northern India famous for its vibrant culture, forts, and palaces.", "rajasthan.png"),
            "Kodaikanal": ("Kodaikanal is a hill town in the southern Indian state of Tamil Nadu. It's set in an area of granite cliffs, forested valleys, lakes, waterfalls, and grassy hills.", "kodaikanal.png"),
            "Ooty": ("Ooty, also known as Udagamandalam, is a hill station in the state of Tamil Nadu, in southern India.", "ooty.png"),
        }

    def get_cuisines_info(self):
        return {
            "Biryanis": ("Biryani is a mixed rice dish originating among the Muslims of the Indian subcontinent.", "biryani.png"),
            "Panipuri": ("Panipuri is a popular street snack in India, consisting of a round, hollow puri filled with a mixture of flavored water, tamarind chutney, chili, chaat masala, potato, onion, and chickpeas.", "panipuri.png"),
            "Chole Bhature": ("Chole bhature is a combination of chana masala and bhatura, a fried bread made from maida flour from the Indian subcontinent.", "chole_bhature.png"),
            "Pav Bhaji": ("Pav bhaji is a fast food dish from India, consisting of a thick vegetable curry, fried and served with a soft bread roll.", "pav_bhaji.png"),
            "Roti": ("Roti is a round flatbread native to the Indian subcontinent made from stoneground wholemeal flour, traditionally known as atta.", "roti.png"),
            "Thali": ("Thali is an Indian meal made up of a selection of various dishes. It is a popular choice for lunch or dinner in India.", "thali.png"),
        }

    def clear_widgets(self):
        # Clear all widgets from the root window
        for widget in self.root.winfo_children():
            widget.destroy()

if __name__ == "__main__":
    root = tk.Tk()
    app = IndocultureApp(root)
    root.mainloop()
